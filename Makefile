COMMON   := -O2 -Wall -Wformat=2 -march=native
CFLAGS   := $(CFLAGS) $(COMMON)
CXXFLAGS := $(CXXFLAGS) $(COMMON)
CC       := gcc
CXX      := g++

TARGETS  := ohwell wordcount

.PHONY : all
all : $(TARGETS)

# {{{ for debugging
DBGFLAGS := -g
debug : CFLAGS += $(DBGFLAGS)
debug : CXXFLAGS += $(DBGFLAGS)
debug : all
.PHONY : debug
# }}}

$(TARGETS) : % : %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $^

.PHONY : clean
clean :
	rm -f $(TARGETS)
