#include <iostream>
using std::cout;
using std::cin;
#include <string>
using std::string;
#include <vector>
using std::vector;
using std::endl;
#include <set>
using std::set;
int numChars;
int numWords = 1;

/*int main (){
    string r;
    r = "this is a test";
    int numWords = 0;
    cout<<r.size()<<endl;
    cout<<r[2]<<endl;
    for(int x = 0; x<r.size(); x++){
        if((r[x] == ' ') && (r[x+1] != ' ')){
        numWords++;
        }
    }
    cout<<numWords<<endl;
    return 0;
} This was a wordcount test to see how we could implement it into the real code*/

//Here is our collective code that I had on VM; the latest version


string line;


int countChars(string s) {
 numChars = s.size() + 1;
 return 0;
}

int countWords(string s) {
 for(unsigned int x = 0; x< s.size();x++) {
  if ((s[x] == ' ') && (s[x+1] != ' '))
   numWords++;
  }
 return numWords;
}

int countUniqueWords(string s) {

 vector<string> wl;
 int numWords = countWords(s);
 int vectLength = 0;
 int tempInit, tempFin;
 int y = 0;

 for (unsigned int y; y < s.size(); y++) {
  if((s[y] != ' ') && (s[y+1] == ' ')) {
   wl.push_back(s.substr(0, y+1));
   vectLength++;
   y++;
   break;
  }
 }
  while(vectLength < (numWords - 1)) {
   if((s[y] == ' ') && (s[y+1] != ' '))
    tempInit = y+1;
   if((s[y] != ' ') && (s[y+1] == ' ')) {
    tempFin = y+1;
    wl.push_back(s.substr(tempInit, tempFin));
    vectLength++;
   }
   y++;
  }
  for(unsigned int y; y< s.size();y++) {
   if((s[y] == ' ') && (s[y+1] != ' ')) {
    wl.push_back(s.substr(y+1));
    vectLength++;
    break;
   }
  }

  for(int z = 0; z < vectLength-1; z++) {

   for(int w = z+1; w < vectLength; w++) {
    if(wl[z] == wl[w]) {
     wl.erase(wl.begin() + w-1);
     vectLength--;
     w--; //accomodate for the removed element
      if(z == vectLength - 1)
       return vectLength;
    }
   }
  }
  return vectLength;
}

int countLines(string s) {
 int numLines = 0;
 for(unsigned int x = 0; x < s.size(); x++) {
  if(s[x] == '\n')
   numLines++;
 return numLines;
 }
 return 0;
}

int countUniqueLines(string s) {
 set<string> uniqueLines;
 string line;
 for(unsigned int x = 0; x<s.size();x++) {
  uniqueLines.insert(line);
 }
 return uniqueLines.size();
}


/*
unsigned long countUniqueWords(const string& s, set<string>& wl){

}*/


int main() {
 while(getline(cin, line)) {
  cout << countLines(line) + '\t';
  cout << countWords(line) + '\t';
  cout << countChars(line) + '\t';
  cout << countUniqueLines(line) + '\t';
  cout << countUniqueWords(line) + '\t';
  if (line =="end"){
   cout<<"\nended."<<endl;
   return 0;
   }
  }
 }
